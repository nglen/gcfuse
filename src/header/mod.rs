mod game;
mod maker;
mod title;

use endian_trait::{prelude::*, BE};
use futures::{AsyncRead, AsyncSeek, AsyncSeekExt};
pub use game::Game;
pub use maker::Maker;
use std::fmt;
use std::io::{self, SeekFrom};
pub use title::Title;

pub struct Header {
    pub game_code: Game,
    pub maker_code: Maker,
    pub disk_id: u8,
    pub version: u8,
    pub audio_streaming: u8,
    pub stream_buffer_size: u8,
    pub game_name: Title,
    pub dh_off: u32,
    pub dh_addr: u32,
    pub dol_off: u32,
    pub fst_off: u32,
    pub fst_size: u32,
    pub fst_max: u32,
    pub user_pos: u32,
    pub user_len: u32,
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "game_code: {}", self.game_code)?;
        writeln!(f, "maker_code: {}", self.maker_code)?;
        writeln!(f, "disk_id: {}", self.disk_id)?;
        writeln!(f, "version: {}", self.version)?;
        writeln!(f, "audio_streaming: {}", self.audio_streaming)?;
        writeln!(f, "stream_buffer_size: {}", self.stream_buffer_size)?;
        writeln!(f, "game_name: {}", self.game_name)?;
        writeln!(f, "dh_off: {}", self.dh_off)?;
        writeln!(f, "dh_addr: {}", self.dh_addr)?;
        writeln!(f, "dol_off: {}", self.dol_off)?;
        writeln!(f, "fst_off: {}", self.fst_off)?;
        writeln!(f, "fst_size: {}", self.fst_size)?;
        writeln!(f, "fst_max: {}", self.fst_max)?;
        writeln!(f, "user_pos: {}", self.user_pos)?;
        write!(f, "user_len: {}", self.user_len)
    }
}

impl Header {
    pub async fn read<R>(r: &mut R) -> io::Result<Self>
    where
        R: AsyncRead + AsyncSeek + Unpin,
    {
        let game_code = Game::read(r).await?;
        let maker_code = Maker::read(r).await?;
        let disk_id = r.read_bytes(BE).await?;
        let version = r.read_bytes(BE).await?;
        let audio_streaming = r.read_bytes(BE).await?;
        let stream_buffer_size = r.read_bytes(BE).await?;

        // unused zeros
        r.seek(SeekFrom::Current(18)).await?;
        // magic (0xc233_9f3d)
        r.seek(SeekFrom::Current(4)).await?;

        let game_name = Title::read(r).await?;
        let dh_off = r.read_bytes(BE).await?;
        let dh_addr = r.read_bytes(BE).await?;

        // unused zeros
        r.seek(SeekFrom::Current(24)).await?;

        let dol_off = r.read_bytes(BE).await?;
        let fst_off = r.read_bytes(BE).await?;
        let fst_size = r.read_bytes(BE).await?;
        let fst_max = r.read_bytes(BE).await?;
        let user_pos = r.read_bytes(BE).await?;
        let user_len = r.read_bytes(BE).await?;

        // unknown
        r.seek(SeekFrom::Current(4)).await?;
        // unused zeroes
        r.seek(SeekFrom::Current(4)).await?;

        Ok(Self {
            game_code,
            maker_code,
            disk_id,
            version,
            audio_streaming,
            stream_buffer_size,
            game_name,
            dh_off,
            dh_addr,
            dol_off,
            fst_off,
            fst_size,
            fst_max,
            user_pos,
            user_len,
        })
    }
}
