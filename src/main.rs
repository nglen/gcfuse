use async_std::{fs::File, path::Path};
use futures::{io::BufReader, lock::Mutex, prelude::*};
use gcfuse::prelude::*;
use gcfuse::types::{Fst, Header};
use polyfuse::{
    io::{Reader, Writer},
    reply::Reply,
    Context, Filesystem, Operation,
};
use std::io::{self, SeekFrom};

fn main() -> io::Result<()> {
    smol::run(async {
        let mut args = std::env::args_os().skip(1);
        let mountpoint = args.next().expect("missing mountpoint");

        let gcfs = Gcfs::new("test.gcm").await?;
        polyfuse_tokio::mount(gcfs, mountpoint, &[]).await?;

        Ok(())
    })
}

pub struct Gcfs {
    file: Mutex<BufReader<File>>,
    fst: Fst,
}

impl Gcfs {
    async fn new(path: impl AsRef<Path>) -> io::Result<Self> {
        let mut file = BufReader::new(File::open(path).await?);
        let header = Header::read(&mut file).await?;

        file.seek(SeekFrom::Start(u64::from(header.fst_off)))
            .await?;
        let fst = Fst::read(&mut file, header.fst_size).await?;

        Ok(Self {
            file: Mutex::new(file),
            fst,
        })
    }
}

async fn try_reply<F, O, T, U>(f: F, cx: &mut Context<'_, U>) -> io::Result<()>
where
    F: FnOnce() -> O,
    O: Future<Output = io::Result<T>>,
    T: Reply,
    U: Writer + Unpin + ?Sized,
{
    match f().await {
        Ok(reply) => cx.reply(reply).await,
        Err(err) => cx.reply_err(err.raw_os_error().unwrap_or(libc::EIO)).await,
    }
}

#[polyfuse::async_trait]
impl Filesystem for Gcfs {
    async fn call<'a, 'cx, T: ?Sized>(
        &'a self,
        cx: &'a mut Context<'cx, T>,
        op: Operation<'cx>,
    ) -> io::Result<()>
    where
        T: Reader + Writer + Send + Unpin,
    {
        macro_rules! try_reply {
            { $($e:stmt);+ } => {
                try_reply(|| async { $($e)+ }, cx).await
            };
        }

        match op {
            Operation::Lookup(op) => try_reply! {
                let parent = self.fst.node(op.parent())?;
                let res = parent.pfuse_lookup(op.name())?;
                Ok(res)
            },
            Operation::Getattr(op) => try_reply! {
                let node = self.fst.node(op.ino())?;
                Ok(node.pfuse_getattr())
            },
            Operation::Readdir(op) => try_reply! {
                let node = self.fst.node(op.ino())?;
                let res = node.pfuse_readdir(op.offset(), op.size())?;
                Ok(res)
            },
            Operation::Read(op) => try_reply! {
                let node = self.fst.node(op.ino())?;
                let (offset, size) = node.to_read()?;

                let mut file = self.file.lock().await;
                let op_off = op.offset();

                if op_off >= u64::from(size) {
                    Ok(Vec::new())
                } else {
                    // seek to the beginning of the file `offset`
                    // plus what we were told `op_off`.
                    let off = u64::from(offset) + op_off;
                    file.seek(SeekFrom::Start(off)).await?;

                    // Avoid reading past the end of the file.
                    let max = u64::from(size) - op_off;
                    let wanted = u64::from(op.size());
                    let size = std::cmp::min(max, wanted) as usize;

                    let mut buf = vec![0; size];
                    file.read_exact(&mut buf).await?;
                    Ok(buf)
                }
            },
            _ => Ok(()),
        }
    }
}
