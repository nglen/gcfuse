// Construct an error that maps to a raw os error.
macro_rules! os_error {
    ($($name:ident => $raw:expr),+) => {
        $(#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
        pub struct $name;

        impl From<$name> for ::std::io::Error {
            fn from(_: $name) -> ::std::io::Error {
                ::std::io::Error::from_raw_os_error($raw)
            }
        })*
    }
}

// Construct an error made up of base errors.
macro_rules! simpl_err {
    ($name:ident => $($entry:ident),*) => {
        #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
        pub enum $name {
            $($entry),*
        }

        // Convert from base errors
        $(impl From<$entry> for $name {
            fn from(_: $entry) -> $name {
                <$name>::$entry
            }
        })*

        // Convert into io::Error
        impl From<$name> for ::std::io::Error {
            fn from(e: $name) -> ::std::io::Error {
                match e {
                    $(<$name>::$entry => $entry.into()),*
                }
            }
        }
    }
}

os_error!(NotADir => libc::ENOTDIR, IsDir => libc::EISDIR, NoEntry => libc::ENOENT);
simpl_err!(FindError => NoEntry, NotADir);
