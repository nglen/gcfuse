use crate::ascii_or_unk;
use futures::{AsyncRead, AsyncReadExt};
use std::{fmt, io};

#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct Maker([u8; 2]);

impl fmt::Display for Maker {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c1 = ascii_or_unk(self.0[0]);
        let c2 = ascii_or_unk(self.0[1]);
        write!(f, "{}{} ({})", c1, c2, self.name())
    }
}

impl Maker {
    pub async fn read<R>(r: &mut R) -> io::Result<Self>
    where
        R: AsyncRead + Unpin,
    {
        let mut buf = [0; 2];
        r.read_exact(&mut buf).await?;
        Ok(Self(buf))
    }

    pub fn name(&self) -> &'static str {
        match &self.0 {
            b"01" => "Nintendo",
            b"08" => "Capcom",
            b"41" => "Ubisoft",
            b"4F" => "Eidos",
            b"51" => "Acclaim",
            b"52" => "Activision",
            b"5D" => "Midway",
            b"5G" => "Hudson",
            b"64" => "LucasArts",
            b"69" => "Electronic Arts",
            b"6S" => "TDK Mediactive",
            b"8P" => "Sega",
            b"A4" => "Mirage Studios",
            b"AF" => "Namco",
            b"B2" => "Bandai",
            b"DA" => "Tomy",
            b"EM" => "Konami",
            _ => "Unknown Publisher",
        }
    }
}
