use crate::ascii_or_unk;
use futures::{AsyncRead, AsyncReadExt};
use std::{fmt, io};

pub struct Title([u8; 992]);

impl fmt::Display for Title {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0
            .iter()
            .cloned()
            .take_while(|&c| c != 0)
            .map(ascii_or_unk)
            .try_for_each(|c| write!(f, "{}", c))
    }
}

impl Title {
    pub async fn read<R>(r: &mut R) -> io::Result<Self>
    where
        R: AsyncRead + Unpin,
    {
        let mut buf = [0; 992];
        r.read_exact(&mut buf).await?;
        Ok(Self(buf))
    }
}
