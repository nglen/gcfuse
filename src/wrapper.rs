#![warn(clippy::all)]

// utilities to make the conversion to a polyfuse filesystem easier
use crate::error;
use crate::fs::{FilesystemNode, Parent, Properties};
use polyfuse::reply::{ReplyAttr, ReplyEntry};
use polyfuse::{DirEntry, FileAttr};
use std::ffi::OsStr;

pub trait PolyfuseHelper: FilesystemNode {
    fn pfuse_attr(&self) -> FileAttr {
        match self.props() {
            Properties::File { size } => {
                let mut attr = FileAttr::default();
                attr.set_mode(libc::S_IFREG as u32 | 0o444);
                attr.set_ino(self.ino());
                attr.set_nlink(1);
                attr.set_uid(unsafe { libc::getuid() });
                attr.set_gid(unsafe { libc::getgid() });
                attr.set_size(u64::from(size));
                attr
            }
            Properties::Directory {} => {
                let mut attr = FileAttr::default();
                attr.set_mode(libc::S_IFDIR as u32 | 0o555);
                attr.set_ino(self.ino());
                attr.set_nlink(1);
                attr.set_uid(unsafe { libc::getuid() });
                attr.set_gid(unsafe { libc::getgid() });
                attr
            }
        }
    }

    // Operation::Lookup
    //
    // op.parent()
    // op.name()
    fn pfuse_lookup(&self, name: &OsStr) -> Result<ReplyEntry, error::FindError> {
        let child = self.find(name)?;

        let mut reply = ReplyEntry::default();
        reply.ino(child.ino());
        reply.attr(child.pfuse_attr());
        Ok(reply)
    }

    // Operation::Getattr
    //
    // op.ino()
    fn pfuse_getattr(&self) -> ReplyAttr {
        ReplyAttr::new(self.pfuse_attr())
    }

    // Operation::Readdir
    //
    // op.ino()
    // op.offset()
    // op.size()
    fn pfuse_readdir(&self, offset: u64, size: u32) -> Result<Vec<DirEntry>, error::NotADir> {
        let mut total_len = 0;
        Ok(std::iter::once((String::from("."), self.ino()))
            .chain(std::iter::once((
                String::from(".."),
                match self.parent() {
                    Parent::RootNode => 1,
                    Parent::File => return Err(error::NotADir),
                    Parent::Directory(parent) => parent.ino(),
                },
            )))
            .chain(self.children()?.map(|v| (v.name(), v.ino())))
            .enumerate()
            .map(|(off, (name, ino))| DirEntry::dir(name, ino, off as u64 + 1))
            .skip(offset as usize)
            .take_while(|entry| {
                total_len += entry.as_ref().len() as u32;
                total_len < size
            })
            .collect())
    }
}

impl<T: FilesystemNode> PolyfuseHelper for T {}
