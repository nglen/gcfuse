mod apploader;
mod dolinfo;
pub mod error;
pub mod fs;
mod fst;
mod header;
mod inode;
pub mod prelude;
mod wrapper;

pub mod types {
    pub use crate::apploader::{AppLoader, APPLOADER_OFF};
    pub use crate::dolinfo::{DolInfo, SectionInfo, DATA_SECTIONS, SECTIONS, TEXT_SECTIONS};
    pub use crate::fst::{Fst, FstNode};
    pub use crate::header::{Game, Header, Maker, Title};
    pub use crate::inode::{INode, INodeKind, INODE_LEN};
}

fn ascii_or_unk(c: u8) -> char {
    if c.is_ascii_graphic() || c == b' ' {
        c as char
    } else {
        '\u{FFFD}'
    }
}
