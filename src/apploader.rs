use endian_trait::{prelude::*, BE};
use std::io;

pub const APPLOADER_OFF: u64 = 0x2440 + 0x10;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct AppLoader {
    pub entry: u32,
    pub code_len: u32,
    pub trail_len: u32,
}

impl AppLoader {
    pub fn read(r: &mut impl io::Read) -> io::Result<Self> {
        let entry = r.read_bytes(BE)?;
        let code_len = r.read_bytes(BE)?;
        let trail_len = r.read_bytes(BE)?;
        Ok(Self {
            entry,
            code_len,
            trail_len,
        })
    }

    pub fn size(&self) -> u32 {
        32 + self.code_len + self.trail_len
    }
}
