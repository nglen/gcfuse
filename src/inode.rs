use endian_trait::{prelude::*, BE};
use futures::{AsyncRead, AsyncReadExt};
use std::io;

pub const INODE_LEN: u32 = 12;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct INode {
    /// This represents the offset into the strings table located immediately after the INodes.
    pub name_off: u32,

    /// File/Directory Specific Properties
    pub kind: INodeKind,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum INodeKind {
    File {
        /// The offset where the file is stored.
        offset: u32,
        /// The size of the stored file.
        size: u32,
    },
    Directory {
        /// The index of the parent directory.
        parent: u32,
        /// The next element of the parent directory.
        next: u32,
    },
}

impl INode {
    pub async fn read<R>(r: &mut R) -> io::Result<Self>
    where
        R: AsyncRead + Unpin,
    {
        let is_dir: u8 = r.read_bytes(BE).await?;
        let name_off = {
            // TODO add 24bit methods
            let mut buf = [0; 4];
            r.read_exact(&mut buf[1..]).await?;
            u32::from_be_bytes(buf)
        };
        let p1 = r.read_bytes(BE).await?;
        let p2 = r.read_bytes(BE).await?;

        let kind = if is_dir != 0 {
            INodeKind::Directory {
                parent: p1,
                next: p2,
            }
        } else {
            INodeKind::File {
                offset: p1,
                size: p2,
            }
        };

        Ok(Self { name_off, kind })
    }
}
