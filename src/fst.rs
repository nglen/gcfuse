use crate::fs::{Filesystem, FilesystemNode, Parent, Properties};
use crate::types::{INode, INodeKind, INODE_LEN};
use crate::{ascii_or_unk, error};
use futures::{AsyncRead, AsyncReadExt};
use std::io;
use std::path::{Component, Path};

pub struct Fst {
    inodes: Vec<INode>,
    strings: Vec<u8>,
}

#[derive(Copy, Clone)]
pub struct FstNode<'a> {
    idx: u32,
    inode: INode,
    fst: &'a Fst,
}

impl Fst {
    pub async fn read<R>(r: &mut R, len: u32) -> io::Result<Self>
    where
        R: AsyncRead + Unpin,
    {
        let root = INode::read(r).await?;
        let num = match root.kind {
            INodeKind::Directory { next, .. } => next,
            INodeKind::File { .. } => 1,
        };

        let mut inodes = Vec::with_capacity(num as usize);
        inodes.push(root);
        for _ in 1..num {
            inodes.push(INode::read(r).await?);
        }

        let rem = len - num * INODE_LEN;
        let mut strings = vec![0; rem as usize];
        r.read_exact(&mut strings).await?;

        Ok(Self { inodes, strings })
    }

    fn string(&self, off: u32) -> String {
        self.strings[off as usize..]
            .iter()
            .cloned()
            .take_while(|&c| c != 0)
            .map(ascii_or_unk)
            .collect()
    }

    fn get_idx(&self, idx: u32) -> Option<FstNode> {
        self.inodes.get(idx as usize).cloned().map(|inode| FstNode {
            idx,
            inode,
            fst: self,
        })
    }
}

impl<'a> Filesystem<'a> for Fst {
    type Node = FstNode<'a>;

    fn node(&'a self, ino: u64) -> Result<Self::Node, error::NoEntry> {
        let idx = (ino - 1) as u32;
        self.get_idx(idx).ok_or(error::NoEntry)
    }
}

impl FstNode<'_> {
    pub fn to_read(&self) -> Result<(u32, u32), error::IsDir> {
        match self.inode.kind {
            INodeKind::File { offset, size } => Ok((offset, size)),
            INodeKind::Directory { .. } => Err(error::IsDir),
        }
    }
}

impl<'a> FilesystemNode for FstNode<'a> {
    type Children = Children<'a>;
    type ChildrenRecurse = ChildrenRecurse<'a>;

    #[inline]
    fn name(&self) -> String {
        self.fst.string(self.inode.name_off)
    }

    #[inline]
    fn props(&self) -> Properties {
        match self.inode.kind {
            INodeKind::Directory { .. } => Properties::Directory {},
            INodeKind::File { size, .. } => Properties::File { size },
        }
    }

    #[inline]
    fn parent(&self) -> Parent<Self> {
        match self.idx {
            0 => Parent::RootNode,
            _ => match self.inode.kind {
                INodeKind::Directory { parent, .. } => {
                    Parent::Directory(self.fst.get_idx(parent).unwrap())
                }
                INodeKind::File { .. } => Parent::File,
            },
        }
    }

    #[inline]
    fn children(&self) -> Result<Self::Children, error::NotADir> {
        match self.inode.kind {
            INodeKind::Directory { next, .. } => Ok(Children {
                cur: self.idx + 1,
                end: next,
                fst: self.fst,
            }),
            INodeKind::File { .. } => Err(error::NotADir),
        }
    }

    #[inline]
    fn traverse(&self) -> Result<Self::ChildrenRecurse, error::NotADir> {
        match self.inode.kind {
            INodeKind::Directory { next, .. } => Ok(ChildrenRecurse {
                range: self.idx + 1..next,
                fst: self.fst,
            }),
            INodeKind::File { .. } => Err(error::NotADir),
        }
    }

    #[inline]
    fn find(&self, path: impl AsRef<Path>) -> Result<Self, error::FindError> {
        path.as_ref()
            .components()
            .try_fold(*self, |cur, comp| match comp {
                Component::RootDir => unimplemented!(),
                Component::CurDir => Ok(cur),
                Component::ParentDir => match cur.parent() {
                    Parent::RootNode => unimplemented!(),
                    Parent::File => Err(error::FindError::NotADir),
                    Parent::Directory(parent) => Ok(parent),
                },
                Component::Normal(name) => cur
                    .children()?
                    .find(|v| Some(v.name().as_str()) == name.to_str())
                    .ok_or(error::FindError::NoEntry),
                Component::Prefix(_) => Err(error::FindError::NoEntry),
            })
    }

    #[inline]
    fn ino(&self) -> u64 {
        u64::from(self.idx) + 1
    }
}

pub struct Children<'a> {
    cur: u32,
    end: u32,
    fst: &'a Fst,
}

impl<'a> Iterator for Children<'a> {
    type Item = FstNode<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cur < self.end {
            let node = self.fst.get_idx(self.cur)?;

            match node.inode.kind {
                INodeKind::Directory { next, .. } => self.cur = next,
                INodeKind::File { .. } => self.cur += 1,
            }

            Some(node)
        } else {
            None
        }
    }
}

pub struct ChildrenRecurse<'a> {
    range: std::ops::Range<u32>,
    fst: &'a Fst,
}

impl<'a> Iterator for ChildrenRecurse<'a> {
    type Item = FstNode<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.range.next().and_then(|idx| self.fst.get_idx(idx))
    }
}
