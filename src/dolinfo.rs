use endian_trait::{prelude::*, BE};
use std::io;

pub const TEXT_SECTIONS: usize = 7;
pub const DATA_SECTIONS: usize = 11;
pub const SECTIONS: usize = TEXT_SECTIONS + DATA_SECTIONS;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct SectionInfo {
    pub offset: u32,
    pub address: u32,
    pub size: u32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct DolInfo {
    pub sec_offs: [u32; SECTIONS],
    pub sec_adrs: [u32; SECTIONS],
    pub sec_lens: [u32; SECTIONS],
    pub bss_adr: u32,
    pub bss_len: u32,
    pub entry: u32,
}

impl DolInfo {
    pub fn read(r: &mut impl io::Read) -> io::Result<Self> {
        let mut sec_offs = [0; SECTIONS];
        let mut sec_adrs = [0; SECTIONS];
        let mut sec_lens = [0; SECTIONS];

        // TODO add slice methods (read_into)
        for v in &mut sec_offs {
            *v = r.read_bytes(BE)?;
        }
        for v in &mut sec_adrs {
            *v = r.read_bytes(BE)?;
        }
        for v in &mut sec_lens {
            *v = r.read_bytes(BE)?;
        }
        let bss_adr = r.read_bytes(BE)?;
        let bss_len = r.read_bytes(BE)?;
        let entry = r.read_bytes(BE)?;

        Ok(Self {
            sec_offs,
            sec_adrs,
            sec_lens,
            bss_adr,
            bss_len,
            entry,
        })
    }

    pub fn sections(&self) -> impl Iterator<Item = SectionInfo> + '_ {
        self.sec_offs
            .iter()
            .zip(self.sec_adrs.iter())
            .zip(self.sec_lens.iter())
            .map(|((&offset, &address), &size)| SectionInfo {
                offset,
                address,
                size,
            })
    }

    pub fn size(&self) -> u32 {
        self.sec_offs
            .iter()
            .zip(self.sec_lens.iter())
            .map(|(a, b)| a + b)
            .max()
            .unwrap_or_default()
    }
}
