use crate::ascii_or_unk;
use futures::{AsyncRead, AsyncReadExt};
use std::{fmt, io};

#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct Game([u8; 4]);

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c1 = ascii_or_unk(self.0[0]);
        let c2 = ascii_or_unk(self.0[1]);
        let c3 = ascii_or_unk(self.0[2]);
        let c4 = ascii_or_unk(self.0[3]);
        write!(f, "{}{}{}{}", c1, c2, c3, c4)
    }
}

impl Game {
    pub async fn read<R>(r: &mut R) -> io::Result<Self>
    where
        R: AsyncRead + Unpin,
    {
        let mut buf = [0; 4];
        r.read_exact(&mut buf).await?;
        Ok(Self(buf))
    }
}
