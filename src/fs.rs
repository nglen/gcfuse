use crate::error;
use std::path::Path;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Parent<T> {
    /// Tried to get the parent of the root node.
    RootNode,
    /// Tried to get the parent of a file.
    File,
    /// The parent of a non-root node.
    Directory(T),
}

#[derive(Debug)]
pub enum Properties {
    File { size: u32 },
    Directory {},
}

/// A node in a given filesystem.
///
/// This common trait is used to represent both files and directories.
pub trait FilesystemNode: Sized {
    type Children: Iterator<Item = Self>;
    type ChildrenRecurse: Iterator<Item = Self>;

    /// Returns the name associated with this node.
    ///
    /// While this function will always return a valid string,
    /// the root node is not guaranteed to give a meaningful one.
    fn name(&self) -> String;

    /// Retrieve some associated properties from the node.
    fn props(&self) -> Properties;

    /// Return the parent node
    fn parent(&self) -> Parent<Self>;

    /// Return an iterator over all direct children of this directory.
    fn children(&self) -> Result<Self::Children, error::NotADir>;

    /// Return an iterator over all direct and indirect children of this directory.
    fn traverse(&self) -> Result<Self::ChildrenRecurse, error::NotADir>;

    /// Return the node associated with a given relative path.
    fn find(&self, path: impl AsRef<Path>) -> Result<Self, error::FindError>;

    /// Returns the associated id of this node.
    fn ino(&self) -> u64;
}

/// A generic filesystem implementation.
///
/// This trait represents a simple read-only filesystem. Each node within the
/// filesystem can be accessed using a unique id stored as a u64. The root node
/// must always be represented as the value 1.
pub trait Filesystem<'a> {
    type Node: FilesystemNode;

    /// Obtain the node with the given id.
    fn node(&'a self, ino: u64) -> Result<Self::Node, error::NoEntry>;
}
